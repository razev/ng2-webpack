import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'my-app',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss']
})

export class AppComponent implements OnInit {
	constructor() {}

	title: string;
	author: string;

	ngOnInit() {
		this.title = 'Welcome to te Future with Angular 2 + Webpack!';
		this.author = 'Renan Azevedo';
	}
}