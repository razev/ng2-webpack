let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let helpers = require('./helpers');

module.exports = {
	entry: {
		'pollyfils': './src/polyfills.ts',
		'vendor': './src/vendor.ts',
		'app': './src/main.ts'
	},

	resolve: {
		extensions: ['', '.ts', '.js']
	},

	module: {
		loaders: [
			{
				test: /\.ts$/,
				loaders: ['awesome-typescript', 'angular2-template']
			},
			{
				test: /\.html$/,
				loader: 'html'
			},
			{
				test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
				loader: 'file?name=assets/[name].[hash].[ext]'
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style', 'to-string!css?sourceMap'),
				exclude: helpers.root('src', 'app')
			},
			{
				test: /\.css$/,
				loader: 'to-string!css?sourceMap'
			},
			// Workaround to proccess third-party scss like bootstrap-sass
			{
				test: /\.scss$/,
				loaders: [ExtractTextPlugin.extract('style', 'raw'), 'to-string', 'css?sourceMap', 'resolve-url', 'sass?sourceMap'],
				exclude: helpers.root('src', 'app')
			},
			{
				test: /\.scss$/,
				loader: 'to-string!css?sourceMap!resolve-url!sass?sourceMap',
				include: helpers.root('src', 'app')
			}
		]
	},

	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: ['app', 'vendor', 'polyfills']
		}),

		new HtmlWebpackPlugin({
			template: 'src/index.html'
		})
	]
};

